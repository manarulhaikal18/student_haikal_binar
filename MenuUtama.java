package latihan1;
import java.util.Scanner;

public class MenuUtama {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		while (true){
			Scanner input = new Scanner(System.in);
			int menu;
			System.out.println("kalkulator penghitung luas dan volume");
			System.out.println("Menu");
			System.out.println("1. hitung luas bidang");
			System.out.println("2. hitung volume");
			System.out.println("0. tutup aplikasi");
			System.out.println("Input menu : ");
			menu = input.nextInt();
	
			if (menu == 1) {
				int jarijari, sisi, alas, tinggi, panjang, lebar;
				double luas;	
			
				System.out.println("pilih bidang yang akan dihitung :");
				System.out.println("1. persegi");
				System.out.println("2. lingkaran");
				System.out.println("3. segitiga");
				System.out.println("4. persegi panjang");
				System.out.println("0. kembali ke menu sebelumnya");
				int menuLuas = input.nextInt();
				if (menuLuas == 1 ) {
					System.out.println("Menghitung Luas Persegi");
					System.out.print("Masukan Sisi: ");
					sisi = input.nextInt();
					luas = sisi * sisi;
					System.out.println("Luas Persegi = "+luas);
				}
				else if(menuLuas == 2) {
					System.out.println("Menghitung Luas Lingkaran");
					System.out.print("Masukan Jari-Jari: ");
					jarijari = input.nextInt();
					luas = 3.14 * (jarijari * jarijari);
					System.out.println("Luas Lingkaran = "+luas);
				}
				else if(menuLuas == 3) {
					System.out.println("Menghitung Luas Segitiga");
					System.out.print("Masukan Alas: ");
					alas = input.nextInt();
					System.out.print("Masukan Tinggi: ");
					tinggi = input.nextInt();
					luas = (alas * tinggi) / 2;
					System.out.println("Luas Segitiga = "+luas);
						
				}
				else if(menuLuas == 4) {
					System.out.println("Menghitung Luas Persegi Panjang");
					System.out.print("Masukan Panjang : ");
					panjang = input.nextInt();
					System.out.print("Masukan Lebar : ");
					lebar = input.nextInt();
					luas = panjang * lebar;
					System.out.println("Luas Persegi Panjang = "+luas);
		
				}
			}
			if (menu == 2) {
				int jarijari, sisi, alas, tinggi, panjang, lebar;
				double volume;
				System.out.println("pilih bidang yang akan dihitung :");
				System.out.println("1. kubus");
				System.out.println("2. balok");
				System.out.println("3. tabung");
				System.out.println("0. kembali ke menu sebelumnya");
				int menuVolume = input.nextInt();
				if (menuVolume == 1) {
					System.out.println("hitung volume kubus: ");
					System.out.print("masukkan sisi kubus: ");
					sisi = input.nextInt();
					volume = sisi*sisi*sisi;
					System.out.println("Volume kubus =" +volume);
				}
				else if (menuVolume == 2) {
					System.out.println("hitung volume balok");
					System.out.println("masukkan panjang: ");
					System.out.println("masukkan lebar: ");
					System.out.println("masukkan tinggi ");
					panjang = input.nextInt();
					lebar = input.nextInt();
					tinggi = input.nextInt();
					volume = panjang*lebar*tinggi;
					System.out.println("Volume Balok =" +volume);
				}
				else if (menuVolume == 3) {
					System.out.println("hitung volune tabung");
					System.out.println("masukkan tinggi: ");
					System.out.println("masukkan jarijari: ");
					tinggi = input.nextInt();
					jarijari = input.nextInt();
					volume = 3.14*tinggi*jarijari*jarijari;
					System.out.println("Volume Tabung=" +volume);
				}
			}
			if (menu == 0) {
				break;		
			}

		}
	}
}